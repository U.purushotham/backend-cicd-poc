set -e

export CI=true

npx semantic-release -t '${version}'
export VERSION=$(git describe --tags --abbrev=0)

if [ -f "version.txt" ]; then
    source version.txt

    docker load -i image/build_artifact.tar
    docker tag build_artifact $CI_REGISTRY/$CI_PROJECT_PATH:$VERSION
    docker push $CI_REGISTRY/$CI_PROJECT_PATH:$VERSION
    docker tag build_artifact $CI_REGISTRY/$CI_PROJECT_PATH:latest
    docker push $CI_REGISTRY/$CI_PROJECT_PATH:latest
fi

sed 's/${IMAGE_VERSION}/'"${VERSION}"'/; s/${NETWORK_NAME}/'"${NETWORK_NAME}"'/' docker-compose-stack-template.yml > docker-compose-stack.yml
