set -e
apk add --no-cache --update nodejs-npm
npm install -g @commitlint/cli @commitlint/config-conventional

commitlint --from=HEAD~1

docker build --build-arg artifactoryContextUrl=$ARTIFACTORY_CONTEXTURL -t build_artifact .
mkdir image
docker save build_artifact > image/build_artifact.tar