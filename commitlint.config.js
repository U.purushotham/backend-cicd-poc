module.exports = {
  extends: ['@commitlint/config-conventional'],
	parserPreset : {
		parserOpts: {
			headerPattern: /^(\w*)\((\w+-[0-9]+)\): (\w*)/,
			headerCorrespondence: ["type","ticket","subject"],
		}
	},
  rules: {
    "subject-case": [0, "never", "subject-case"],
  }
};
